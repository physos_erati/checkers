document.addEventListener("DOMContentLoaded", () => {
	const pieces = document.querySelectorAll("#board .piece");
	const squares = document.querySelectorAll("#board .black");
	
	let pieceSelected = false;
	let currentPlayer = "black";
	
	const selectPiece = (event) => {
		const currentPiece = event.target;

		if(
			!pieceSelected &&
			currentPiece.classList.contains("piece-" + currentPlayer)
		) {
			currentPiece.classList.add("selected");
			pieceSelected = true;

			for(let j = 0; j < squares.length; j++) {
				if(squares[j].children.length === 0) {
					squares[j].addEventListener("click", moveTo);

					squares[j].origin = currentPiece;
					squares[j].destination = squares[j];
				}
			}
			
			document.querySelector("#board .selected").addEventListener("click", uncheckPiece);
		}
	}

	const uncheckPiece = function() {
		this.classList.remove("selected");
		pieceSelected = false;

		this.removeEventListener("click", uncheckPiece);
	}

	const moveTo = (event) => {

		const origin = event.target.origin;
		const destination = event.target.destination;

		for(let i = 0; i < squares.length; i++) {
			if(squares[i].children.length === 0) {
				squares[i].removeEventListener("click", moveTo);
			}
		}

		destination.innerHTML = '<div class="piece piece-' +currentPlayer+ '"></div>';
		destination.lastElementChild.addEventListener("click", selectPiece);
		origin.remove();
		pieceSelected = false;

		currentPlayer = (currentPlayer == "black") ? "white" : "black";
	}

	for(let i = 0; i < pieces.length; i++) {
		pieces[i].addEventListener("click", selectPiece);
	}
});